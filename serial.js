var serialport = require("serialport");
var SerialPort = serialport.SerialPort

xivelyParams = {
  key: 'xUC2caUdurVWvyvbFtdGtUEm7YQtdTGQiOQzMPiU57LdRXuN',
  channel: '826420357'
}

serialParams = {
  port: '/dev/ttyACM0',
  baud: 115200
}

var serialPort = new SerialPort(serialParams.port, {
    baudrate: serialParams.baud,
    parser: serialport.parsers.readline("\n")
});

serialPort.open(function (error) {
  if ( error ) {
    console.log('failed to open: '+error);
  } else {
    serialPort.on('data', function(data) {
      try {
        data_json = JSON.parse(data);
      }
      catch (e) {
       return;
      }
      console.log(data_json);
      feed_xively(data_json)
    });
  }
});


var XivelyClient = require('xively');
var x = new XivelyClient();
x.setKey(xivelyParams.key);

function feed_xively(data) {
  var datastreams = [];
  for (key in data) {
    datastreams.push({
      "id": key,
      "current_value": data[key]
    });
  }
  // console.log(datastreams);
  var pH = data.pH;
  var filtedPH = data.filtedPH;
  var dp = {
    "version":"1.0.0",
    "datastreams" : datastreams
  }
  x.feed.new(xivelyParams.channel, {
    data_point: dp,
    callback: function(e) {}
  });
}
